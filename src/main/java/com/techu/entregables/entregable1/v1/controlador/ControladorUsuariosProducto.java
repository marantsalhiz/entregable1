package com.techu.entregables.entregable1.v1.controlador;

import com.techu.entregables.entregable1.v1.modelo.ModeloProducto;
import com.techu.entregables.entregable1.v1.modelo.ModeloUsuario;
import com.techu.entregables.entregable1.v1.servicio.ServicioDatos;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("${api.version}/productos")
public class ControladorUsuariosProducto {

    @Autowired
    private ServicioDatos servicioDatos;

    //GET - DEVOLVER LOS USUARIOS DE UN PRODUCTO
    @GetMapping("/{idProducto}/usuarios")
    public ResponseEntity obtenerUsuariosProducto(@PathVariable int idProducto) {
        try {
            return ResponseEntity.ok(this.servicioDatos.obtenerUsuariosProducto(idProducto));
        } catch (Exception x) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    //GET - DEVOLVER UN USUARIO DE UN PRODUCTO
    @GetMapping("/{idProducto}/usuarios/{idUsuario}")
    public ResponseEntity obtenerUsuarioProducto(@PathVariable int idProducto, @PathVariable int idUsuario) {
        final ModeloUsuario u = this.servicioDatos.obtenerUsuarioProducto(idProducto, idUsuario);
        return (u == null)
                ? new ResponseEntity(HttpStatus.NOT_FOUND)
                : ResponseEntity.ok(u);
    }

    //POST - INSERTAR UN USUARIO DE UN PRODUCTO
    @PostMapping("/{idProducto}/usuarios")
    public ResponseEntity agregarUsuarioProducto(@PathVariable int idProducto, @RequestBody ModeloUsuario usuario) {
        final ModeloProducto p = this.servicioDatos.obtenerProductoPorId(idProducto);
        if (p == null)
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        return ResponseEntity.ok(this.servicioDatos.agregarUsuarioProducto(idProducto, usuario));
    }

    //DELETE - ELIMINAR UN USUARIO DE UN PRODUCTO  -- //IDEMPOTENTE
    @DeleteMapping("{idProducto}/usuarios/{idUsuario}")
    public ResponseEntity borrarUsuarioProducto(@PathVariable int idProducto, @PathVariable int idUsuario) {
        this.servicioDatos.borrarUsuarioProducto(idProducto,idUsuario);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    //PUT - REEMPLAZAR UN USUARIO DE UN PRODUCTO
    @PutMapping("/{idProducto}/usuarios/{idUsuario}")
    public ResponseEntity reemplazarProductoUsuario(@PathVariable int idProducto, @PathVariable int idUsuario, @RequestBody ModeloUsuario usuario) {
        final boolean qq = this.servicioDatos.reemplazarProductoUsuario(idProducto, idUsuario, usuario);
        if(qq == true) {
            return ResponseEntity.ok(qq);
        }
        return new ResponseEntity(HttpStatus.NOT_FOUND);
    }


}

