package com.techu.entregables.entregable1.v1.servicio;

import com.techu.entregables.entregable1.v1.modelo.ModeloProducto;
import com.techu.entregables.entregable1.v1.modelo.ModeloUsuario;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

//CRUD de Productos
//Create
//Read
//Update
//Delete

@Component  // se le dice a sprint que lo va manejar él
public class ServicioDatos {
    private final AtomicInteger secuenciaIdsProductos = new AtomicInteger(0);
    private final AtomicInteger secuenciaIdsUsuarios = new AtomicInteger(0);

    private final List<ModeloProducto> productos = new ArrayList<ModeloProducto>();

    //--- PRODUCTOS:

    //GET - PRODUCTOS
    public List<ModeloProducto> obtenerProductos() {
        return Collections.unmodifiableList(this.productos);
    }

    //GET - UN PRODUCTO
    public ModeloProducto obtenerProductoPorId(int id) {
        for (ModeloProducto p : this.productos) {
            if (p.getId() == id)
                return p; //OJO: se puede modificar desde afuera
        }
        return null;
    }

    //POST - PRODUCTO
    public ModeloProducto agregarProducto(ModeloProducto producto) {
        producto.setId(this.secuenciaIdsProductos.incrementAndGet());
        this.productos.add(producto);
        return producto;
    }

    //DELETE - UN PRODUCTO
    public boolean borrarProducto(int id) {
        for (int i = 0; i < this.productos.size(); ++i) {
            if (this.productos.get(i).getId() == id) {
                this.productos.remove(i);
                return true;
            }
        }
        return false;
    }

    //PUT - UN PRODUCTO
    public boolean reemplazarProducto(int id, ModeloProducto producto) {
        for (int i = 0; i < this.productos.size(); ++i) {
            if (this.productos.get(i).getId() == id) {
                producto.setId(id);
                this.productos.set(i, producto);
                return true;
            }
        }
        return false;
    }

    //PATCH - MODIFICAR MARCA, DESCRIPCION Ó PRECIO DE UN PRODUCTO
    public boolean modificarProducto(int id, ModeloProducto producto) {
        final ModeloProducto p = this.obtenerProductoPorId(id);
        if (p == null) {
            return false;
        }
        if (producto.getMarca() != null) {
            p.setMarca(producto.getMarca());
        }
        if (producto.getDescripcion() != null) {
            p.setDescripcion(producto.getDescripcion());
        }
        if (producto.getPrecio() > 0.0) {
            p.setPrecio(producto.getPrecio());
        }
        return true;
    }

    //---PRODUCTO - USUARIOS:

    //GET - USUARIOS DE UN PRODUCTO
    public List<ModeloUsuario> obtenerUsuariosProducto(int idProducto) {
        return Collections.unmodifiableList(this.obtenerProductoPorId(idProducto).getUsuarios());
    }

    //GET - UN USUARIO DE UN PRODUCTO
    public ModeloUsuario obtenerUsuarioProducto(int idProducto, int idUsuario) {
        final ModeloProducto p = this.obtenerProductoPorId(idProducto);
        if (p == null)
            return null;
        for (ModeloUsuario u : p.getUsuarios()) {
            if (u.getId() == idUsuario) {
                return u;  //OJO: se puede modificar desde afuera
            }
        }
        return null;
    }

    //POST - UN USUARIO A UN PRODUCTO
    public ModeloUsuario agregarUsuarioProducto(int idProducto, ModeloUsuario usuario) {
        usuario.setId(this.secuenciaIdsUsuarios.incrementAndGet());
        this.obtenerProductoPorId(idProducto).getUsuarios().add(usuario);
        return usuario;
    }

    //DELETE - UN USUARIO DE UN PRODUCTO
    public boolean borrarUsuarioProducto(int idProducto, int idUsuario) {
        final ModeloProducto producto = this.obtenerProductoPorId(idProducto);
        if (producto == null)
            return false;
        final List<ModeloUsuario> usuarios = producto.getUsuarios();
        for (int i = 0; i < usuarios.size(); ++i) {
            if (usuarios.get(i).getId() == idUsuario) {
                usuarios.remove(i);
                return true;
            }
        }
        return false;
    }

    //PUT - REEMPLAZAR UN USUARIO DE UN PRODUCTO
    public boolean reemplazarProductoUsuario(int idProducto, int idUsuario, ModeloUsuario usuario) {
        final ModeloProducto producto = this.obtenerProductoPorId(idProducto);
        if (producto == null)
            return false;
        final List<ModeloUsuario> usuarios = producto.getUsuarios();
        for (int i = 0; i < usuarios.size(); ++i) {
            if (usuarios.get(i).getId() == idUsuario) {
                usuario.setId(idUsuario);
                usuarios.set(i,usuario);
                return true;
            }
        }
        return false;
    }

 }
