package com.techu.entregables.entregable1.v1.controlador;

import com.techu.entregables.entregable1.v1.modelo.ModeloProducto;
import com.techu.entregables.entregable1.v1.servicio.ServicioDatos;
import org.springframework.http.ResponseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/productos")
public class ControladorProductos {

    @Autowired //SPRING TRAE ServivioDatos para ser usado sus objetos como SPRING
    private ServicioDatos servicioDatos;

    //GET - DEVOLVER TODOS LOS PRODUCTOS INCLUYENDO TODOS SUS USUARIOS
    @GetMapping
    public ResponseEntity obtenerProductos() {
        return ResponseEntity.ok(this.servicioDatos.obtenerProductos());
    }

    //GET - DEVOLVER UN PRODUCTO CON EL ID INCLUYENDO TODOS SUS USUARIOS
    @GetMapping("/{id}")
    public ResponseEntity obtenerUnProducto(@PathVariable int id) {
        final ModeloProducto p = this.servicioDatos.obtenerProductoPorId(id);
        if(p != null) {
            return ResponseEntity.ok(p);
        }
        return new ResponseEntity(HttpStatus.NOT_FOUND);
    }

    //POST - INSERTAR UN PRODUCTO
    @PostMapping
    public ResponseEntity crearProducto(@RequestBody ModeloProducto producto) {
        final ModeloProducto p = this.servicioDatos.agregarProducto(producto);
        return ResponseEntity.ok(p);
    }

    //DELETE - ELIMINAR UN PRODUCTO  ----- //IDEMPOTEMTE
    @DeleteMapping("/{id}")
    public ResponseEntity borraUnProducto(@PathVariable int id) {
        this.servicioDatos.borrarProducto(id);
        return new ResponseEntity("¡Eliminado!", HttpStatus.NO_CONTENT);
    }

    //PUT - REEMPLAZAR UN PRODUCTO
    @PutMapping("/{id}")
    public ResponseEntity reemplazarProducto(@PathVariable int id, @RequestBody ModeloProducto producto) {
        final boolean qq = this.servicioDatos.reemplazarProducto(id, producto);
        if(qq == true) {
            return ResponseEntity.ok(qq);
        }
        return new ResponseEntity(HttpStatus.NOT_FOUND);
    }

    //PATCH - MODIFICAR MARCA, DESCRIPCION Ó PRECIO DE UN PRODUCTO
    @PatchMapping("/{id}")
    public ResponseEntity modificarProducto(@PathVariable int id, @RequestBody ModeloProducto producto) {
        final boolean qq = this.servicioDatos.modificarProducto(id, producto);
        if(qq == true) {
            return ResponseEntity.ok(qq);
        }
        return new ResponseEntity(HttpStatus.NOT_FOUND);
    }

}
